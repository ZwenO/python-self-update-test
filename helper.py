import shutil


def delete_folder(pth):
    shutil.rmtree(pth, ignore_errors=True)
    