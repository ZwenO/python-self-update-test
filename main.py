import urllib.request
import os
import sys
import zipfile
import helper

path = os.path.dirname(__file__)

tempDirectory = path + '\\tmp'

with urllib.request.urlopen("https://gitlab.com/ZwenO/python-self-update-test/-/archive/main/python-self-update-test-main.zip") as zipFileRequest:
    helper.delete_folder(tempDirectory)
    os.mkdir(tempDirectory)
    zipFile = tempDirectory + '\\newVersion.zip'
    with open(zipFile, "wb+") as f:
        f.write(zipFileRequest.read())

    with zipfile.ZipFile(zipFile, 'r') as zip_ref:
        zip_ref.extractall(path + '\\..')


#if checksum local != remote
#update
#execute new and exit
#os.execl(sys.argv[0], *sys.argv)
#exit()
